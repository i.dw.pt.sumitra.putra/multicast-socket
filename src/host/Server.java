package host;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Server {
    int port;
    String address;
    String serverName;
    MulticastSocket server;
    InetAddress inetAddress;

    public Server(String address, int port, String serverName) throws IOException {
        System.out.println("SERVER: " + serverName);
        System.out.println("ADDRESS: " + address + "; PORT: " + port + ";");
        this.inetAddress = InetAddress.getByName(address);
        this.server = new MulticastSocket(port);
        this.server.joinGroup(this.inetAddress);
        this.address = address;
        this.port = port;
        this.serverName = serverName;
    }

    public void send(String message) throws IOException {
        byte[] messagesBytes = message.getBytes();
        int messageLength = messagesBytes.length;
        DatagramPacket outPacket = new DatagramPacket(messagesBytes, messageLength, inetAddress, this.port);
        this.server.send(outPacket);
    }

    public String getServerName() {
        return serverName;
    }
}
