package host;

import model.Constant;
import model.Message;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Client {
    int port;
    String address;
    String username;
    MulticastSocket client;
    InetAddress inetAddress;
    ArrayList<Message> messageFlag;

    public Client(String address, int port, String username) throws IOException {
        System.out.println("CLIENT: " + username);
        System.out.println("ADDRESS: " + address + "; PORT: " + port + ";");
        this.inetAddress = InetAddress.getByName(address);
        this.client = new MulticastSocket(port);
        this.client.joinGroup(this.inetAddress);
        this.address = address;
        this.port = port;
        this.username = username;
        this.messageFlag = new ArrayList<>();
    }

    public void listen() throws IOException {
        while (true) {
            System.out.println("\n\nWaiting for data to receive...");
            byte[] messageBuffer = new byte[Constant.MESSAGE_BUFFER];
            int messageBufferLength = messageBuffer.length;
            DatagramPacket datagramPacket = new DatagramPacket(messageBuffer, Constant.MESSAGE_BUFFER, this.inetAddress, this.port);
            this.client.receive(datagramPacket);
            String rawMessage = new String(messageBuffer, 0, messageBufferLength);
            Message message = Message.convertToMessage(rawMessage);
            System.out.println("Received Message: " + message.getFullMessage());

            Boolean isAlreadySendBefore = this.checkOrFlag(message);
            if (isAlreadySendBefore) {
                this.printSenders();
                continue;
            }

            Boolean isReceiver = message.isReceiver(this.username);
            if (isReceiver) {
                System.out.println("Target is me! Already received the message!");
                continue;
            }

            Boolean isSender = message.isSender(this.username);
            if (isSender) {
                System.out.println("Flooding");
                continue;
            }

            try {
                Message nextMessage = message.getNextMessage(this.username);
                if (nextMessage.isDropMessage(this.username)) {
                    System.out.println("This message is dropped: " + nextMessage.getFullMessage());
                    break;
                }
                System.out.println("Re-Sending the message...");
                System.out.println("Since its NOT for me!\n\n");
                this.send(nextMessage);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public Boolean checkOrFlag(Message incomingMessage) {
        List<Message> flaggedMessage = this.messageFlag
                .stream()
                .filter(message -> message.equals(incomingMessage))
                .collect(Collectors.toList());
        Boolean isAlreadySendBefore = flaggedMessage.size() > 0;
        System.out.println(isAlreadySendBefore);
        if (!isAlreadySendBefore) {
            this.messageFlag.add(incomingMessage);
        }
        return isAlreadySendBefore;
    }

    public void printSenders() {
        this.messageFlag.forEach(message -> System.out.println(message.getFrom()));
    }

    public void send(Message message) throws IOException {
        byte[] messagesBytes = message.getFullMessage().getBytes();
        int messageLength = messagesBytes.length;
        DatagramPacket outPacket = new DatagramPacket(messagesBytes, messageLength, inetAddress, this.port);
        this.client.send(outPacket);
    }

    public String getUsername() {
        return username;
    }
}
