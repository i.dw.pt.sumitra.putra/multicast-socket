package multicast;

import host.Server;
import model.Message;

import java.io.IOException;
import java.util.Scanner;

import static model.Constant.INET_ADDR;
import static model.Constant.PORT;

public class ServerMain {

    private static Message createMessage(String username) {
        Scanner in = new Scanner(System.in);
        System.out.print("message.Message: ");
        String messageText = in.nextLine();
        System.out.print("Receiver: ");
        String to = in.nextLine();
        System.out.print("TTL: ");
        Integer ttl = Integer.parseInt(in.nextLine());
        System.out.print("message.Message lifetime: ");
        Integer messageLifetime = Integer.parseInt(in.nextLine());

        return new Message(username, to, messageText, ttl, messageLifetime);
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server(INET_ADDR, PORT, "MAIN_SENDER");

        try {
            String user = server.getServerName();
            Message message = createMessage(user);
            String fullMessage = message.getFullMessage();
            System.out.println("Full Message: " + fullMessage);
            if (message.isDropMessage(user)) {
                System.out.println("Message is dropped!");
                return;
            }

            System.out.println("Sending message...");
            server.send(fullMessage);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
