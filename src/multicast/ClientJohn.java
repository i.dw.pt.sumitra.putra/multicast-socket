package multicast;

import host.Client;
import model.Constant;

import java.io.IOException;

public class ClientJohn {

    public static String USERNAME = "CLIENT_JOHN";

    public static void main(String[] args) throws IOException {
        Client client = new Client(Constant.INET_ADDR, Constant.PORT, USERNAME);
        client.listen();
    }
}
