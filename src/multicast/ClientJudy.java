package multicast;

import host.Client;
import model.Constant;

import java.io.IOException;

public class ClientJudy {

    public static String USERNAME = "CLIENT_JUDY";

    public static void main(String[] args) throws IOException {
        Client client = new Client(Constant.INET_ADDR, Constant.PORT, USERNAME);
        client.listen();
    }
}
