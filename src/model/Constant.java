package model;

public class Constant {
    public static final String INET_ADDR = "224.0.0.3";
    public static final Integer PORT = 2526;
    public static final Integer MESSAGE_BUFFER = 10000;
}
