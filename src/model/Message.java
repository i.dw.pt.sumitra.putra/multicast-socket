package model;

import java.util.Objects;

public class Message {
    private String from;
    private String to;
    private String message;
    private Integer ttl;
    private Integer lifetime;
    private final String DELIMITER = "-";

    public Message() { }

    public Message(String from, String to, String message, Integer ttl, Integer lifetime) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.ttl = ttl;
        this.lifetime = lifetime;
    }

    public String getFullMessage() {
        String ttlString = Integer.toString(this.ttl);
        String lifetimeString = Integer.toString(this.lifetime);
        return String.join(DELIMITER, from, to, message, ttlString, lifetimeString);
    }

    public Boolean isDropMessage(String currentSender) {
        return this.ttl < 0 || this.lifetime < 0 || currentSender.equalsIgnoreCase(to);
    }

    public Boolean isReceiver(String receivers) {
        return receivers.equalsIgnoreCase(to);
    }

    public Boolean isSender(String sender) {
        return sender.equalsIgnoreCase(from);
    }

    public Message getNextMessage(String nextSender) {
        Integer newTtl = this.ttl - 1;
        Integer newLifetime = this.lifetime - 300;
        return new Message(nextSender, this.to, this.message, newTtl, newLifetime);
    }

    public static Message convertToMessage(String rawMessage) {
        String rawData[] = rawMessage.split("-");
        String from = rawData[0];
        String to = rawData[1];
        String message = rawData[2];
        Integer ttl = Integer.parseInt(rawData[3].trim());
        Integer lifetime = Integer.parseInt(rawData[4].trim());
        return new Message(from, to, message, ttl, lifetime);
    }

    public String getFrom() {
        return from;
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) return true;
        if (otherObject == null || getClass() != otherObject.getClass()) return false;

        Message otherMessage = (Message) otherObject;

        if (!Objects.equals(to, otherMessage.to)) return false;
        return Objects.equals(message, otherMessage.message);
    }
}
