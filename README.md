# multicast-socket
Multicast message sender and receiver. Message send via broadcast to all connected listener. Client must send the updated message if its not for them. Client prevent re sending the message if they already sent it before. To prevent message flooding in network
